# (un)define the next line to either build for the newest or all current kernels
#define buildforkernels newest
%define buildforkernels akmod

%define         tarbase synaptics-usb
%define         tarvers 1.5rc4

Name:           synaptics-usb-kmod
Version:        1.5
Release:        4%{?tarvers:.%{tarvers}}%{?dist}
Summary:        USB Synaptics device driver - kernel driver

Group:          System Environment/Kernel
License:        GPLv2+
URL:            http://www.jan-steinhoff.de/linux/synaptics-usb.html
Source0:        http://www.jan-steinhoff.de/linux/%{tarbase}-%{tarvers}.tar.bz2
Source11:       synaptics-usb-kmodtool-excludekernel-filterfile
Patch1:         synaptics-usb-kmod-Makefile.patch
BuildRoot:      %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)








%define         repo rpmfusion

# get the needed BuildRequires (in parts depending on what we build for)
BuildRequires:  %{_bindir}/kmodtool
Requires:       akmods
%{!?kernels:BuildRequires: buildsys-build-%{repo}-kerneldevpkgs-%{?buildforkernels:%{buildforkernels}}%{!?buildforkernels:current}-%{_target_cpu} }
# kmodtool does its magic here
%{expand:%(kmodtool --target %{_target_cpu} --repo %{repo} --kmodname %{name} --filterfile %{SOURCE11} %{?buildforkernels:--%{buildforkernels}} %{?kernels:--for-kernels "%{?kernels}"} 2>/dev/null) }


%description
USB Synaptics device driver - kernel driver


%prep
# error out if there was something wrong with kmodtool
%{?kmodtool_check}
# print kmodtool output for debugging purposes:
kmodtool --target %{_target_cpu} --repo %{repo} --kmodname %{name} --filterfile %{SOURCE11} %{?buildforkernels:--%{buildforkernels}} %{?kernels:--for-kernels "%{?kernels}"} 2>/dev/null
%setup -q -c -T -a 0
%patch1 -p0 -b.makefile

for kernel_version in %{?kernel_versions}; do
    %{__cp} -rl synaptics-usb _kmod_source_${kernel_version%%___*}
done







%define module_list synaptics-usb


%build
for kernel_version in %{?kernel_versions}; do
    mkdir "_kmod_build_${kernel_version%%___*}"
    pushd "_kmod_source_${kernel_version%%___*}"
    %{__make} \
               V=1 \
               O="_kmod_build_${kernel_version%%___*}" \
               KVER="${kernel_version%%___*}" \
               KSRC="${kernel_version##*___}" \
               KBUILD="${kernel_version##*___}" \
               modules
    popd
    for mod in %{module_list}; do
        mv -f "_kmod_source_${kernel_version%%___*}/${mod}.ko" "_kmod_build_${kernel_version%%___*}"
    done
    mv -f "_kmod_source_${kernel_version%%___*}/Module.symvers" "_kmod_build_${kernel_version%%___*}"
done


%install
%{__rm} -rf "%{buildroot}"
for kernel_version in %{?kernel_versions}; do
  for module in %{module_list}; do
    %{__install} -p -D -m 0755 \
        "_kmod_build_${kernel_version%%___*}/${module}.ko" \
        "%{buildroot}%{kmodinstdir_prefix}/${kernel_version%%___*}/%{kmodinstdir_postfix}/${module}.ko"
  done
done


%{?akmod_install}


%clean
%{__rm} -rf "%{buildroot}"


%changelog
* Sun Apr 12 2009 Hans Ulrich Niedermann <hun@n-dimensional.de> - 1.5-4.1.5rc4
- fix building of kernel module via "service akmod restart"
- first working package

* Sun Apr 12 2009 Hans Ulrich Niedermann <hun@n-dimensional.de> - 1.5-3.1.5rc4
- bump version to stay in sync with synaptics-usb

* Sun Apr 12 2009 Hans Ulrich Niedermann <hun@n-dimensional.de> - 1.5-2.1.5rc4
- bump version to stay in sync with synaptics-usb

* Sun Apr 12 2009 Hans Ulrich Niedermann <hun@n-dimensional.de> - 1.5-0.1.5rc4
- initial package
